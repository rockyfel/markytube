import { Component, OnInit } from '@angular/core';
import { isNull, isNumber } from 'util';
import { VideoPreview } from '../VideoPreview';
import { IVideo } from '../interfaces/IVideo';
import { IMyVideo } from '../interfaces/IMyVideo';
import { VideoService } from '../services/video.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-animies',
  templateUrl: './animies.page.html',
  styleUrls: ['./animies.page.scss'],
})
export class AnimiesPage extends VideoPreview implements OnInit{
  public Animies: IMyVideo;
  constructor(
    public videoService: VideoService,
    private router: Router
  ) { 
    super();
  }

  ngOnInit():void{
    this.GetAnimies(); 
    this.videoService.SelectedVideo = null;
    //let id = new URL(document.URL).searchParams.get("id");
    
    // if(!isNull(id) && !Number.isNaN(parseInt(id))){
    //   this.GetSelectedAnime(id);
    // }
  }

  GetAnimies(){
    this.Animies = {
      VideoList: this.videoService.GetAnimies()
    }
  }

}
