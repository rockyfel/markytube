import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AnimiesPage } from './animies.page';
import { PlayVideoComponent } from '../play-video/play-video.component';

const routes: Routes = [
  {
    path: '',
    component: AnimiesPage,
    children: [
      {
        path: ':id',
        component: PlayVideoComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AnimiesPageRoutingModule {}
