import { Component, OnInit } from '@angular/core';
import { isNull, isNumber } from 'util';
import { VideoPreview } from '../VideoPreview';
import { IMyVideo } from '../interfaces/IMyVideo';
import { IVideo } from '../interfaces/IVideo';
import { VideoService } from '../services/video.service';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.page.html',
  styleUrls: ['./movies.page.scss'],
})

export class MoviesPage extends VideoPreview implements OnInit {
  public Movies : IMyVideo;

  constructor(public videoService: VideoService) { 
    super();
  }

  ngOnInit(){
    this.GetMovies();
    this.videoService.SelectedVideo = null;
  }

  GetMovies(){
    this.Movies = {
      VideoList: this.videoService.GetMovies()
    }
  }

}
