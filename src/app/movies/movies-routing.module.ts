import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MoviesPage } from './movies.page';
import { PlayVideoComponent } from '../play-video/play-video.component';

const routes: Routes = [
  {
    path: '',
    component: MoviesPage,
    children: [
      {
        path: ':id',
        component: PlayVideoComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MoviesPageRoutingModule {}
