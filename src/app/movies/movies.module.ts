import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MoviesPageRoutingModule } from './movies-routing.module';

import { MoviesPage } from './movies.page';
import { MenuModule } from '../menu/menu.module';
import { PlayVideoModule } from '../play-video/play-video.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MenuModule,
    MoviesPageRoutingModule,
    PlayVideoModule
  ],
  declarations: [MoviesPage]
})
export class MoviesPageModule {}
