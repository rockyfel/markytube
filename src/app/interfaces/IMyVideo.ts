import { IVideo } from './IVideo';

export interface IMyVideo {
    VideoList: IVideo[];
    SelectedVideo?: IVideo;
}