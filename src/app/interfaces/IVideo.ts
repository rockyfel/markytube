export interface IVideo {
    FileName: string, 
    Path: string,
    VidType: string, 
    Thumbnail: string,
    Content: string,
    VidId: Number
}