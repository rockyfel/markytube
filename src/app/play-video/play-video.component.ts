import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { IMyVideo } from '../interfaces/IMyVideo';
import { IVideo } from '../interfaces/IVideo';
import { VideoService } from '../services/video.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-play-video',
  templateUrl: './play-video.component.html',
  styleUrls: ['./play-video.component.scss'],
})
export class PlayVideoComponent implements OnInit {

  constructor(
    public videoService: VideoService,
    private activatedRouter: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    let id = this.activatedRouter.snapshot.paramMap.get('id');
    this.GetSelected(Number(id));
  }

  GetSelected(id:Number):void{
    this.videoService.SelectedVideo = this.videoService.GetExistingVideoList().find(element => element.VidId == id);
  }

}
