import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { PlayVideoComponent } from '../play-video/play-video.component';

@NgModule({
  declarations: [PlayVideoComponent],
  imports: [
    CommonModule,
    RouterModule,
    IonicModule
  ],
  exports: [PlayVideoComponent]
})
export class PlayVideoModule { }
