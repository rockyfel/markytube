export class VideoPreview {
    ShowPreview(element) {
        let _vidId = element.getElementsByClassName("vid-thumbnail")[0].getAttribute("data-id");
        let _vidPath = element.getElementsByClassName("vid-thumbnail")[0].getAttribute("data-info");
        element.getElementsByClassName("vid-thumbnail")[0].setAttribute("id", "active-vid-thumbnail");
        element.getElementsByClassName("vid-thumbnail")[0].style.display = "none";
        let vid = document.getElementById(_vidId).insertAdjacentHTML("beforeend","<video id='_" + _vidId + "' width='209' height='117' muted autoplay><source src='" + _vidPath + "' type='video/mp4'><source src='" + _vidPath + "' type='video/ogg'><source src='" + _vidPath + "' type='video/webm'>Your browser does not support the video tag.</video>");
    }

    HidePreview() {
        let r_vidId = document.getElementById("active-vid-thumbnail").getAttribute("data-id");
        /* remove the video in thumbnail container */
        document.getElementById("_" + r_vidId).remove();
        /* display again the image in thumbnail container */
        let active_vid = document.getElementById("active-vid-thumbnail");
        active_vid.style.display = "block";
        active_vid.removeAttribute("id");
    }
}