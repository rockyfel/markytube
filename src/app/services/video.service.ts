import { Injectable } from '@angular/core';
import { IVideo } from '../interfaces/IVideo';

@Injectable({
  providedIn: 'root'
})
export class VideoService {
  private _VideoList: IVideo[];
  public SelectedVideo: IVideo;

  constructor() { }

  GetAnimies(): IVideo[]{
    return this._VideoList = [
      {FileName:"mov_bbb", 
      Path:"assets/mov_bbb.mp4",
      VidType:"mp4", 
      Thumbnail:"assets/MarkyTube.png",
      Content:"Test Title Anime 1",
      VidId:1},
      {FileName:"bb_bunny", 
      Path:"assets/bb_bunny.webm",
      VidType:"webm", 
      Thumbnail:"assets/MarkyTube.png",
      Content:"Test Title Anime 2",
      VidId:2},
      {FileName:"bb_bunny", 
      Path:"assets/bb_bunny.webm",
      VidType:"webm", 
      Thumbnail:"assets/MarkyTube.png",
      Content:"Test Title Anime 3",
      VidId:3},
      {FileName:"bb_bunny", 
      Path:"assets/bb_bunny.webm",
      VidType:"webm", 
      Thumbnail:"assets/MarkyTube.png",
      Content:"Test Title Anime 4",
      VidId:4}
    ];
  }

  GetMovies(): IVideo[]{
    return this._VideoList = [
      {FileName:"mov_bbb", 
      Path:"assets/mov_bbb.mp4",
      VidType:"mp4", 
      Thumbnail:"assets/MarkyTube.png",
      Content:"Test Title Movie 1",
      VidId:1},
      {FileName:"bb_bunny", 
      Path:"assets/bb_bunny.webm",
      VidType:"webm", 
      Thumbnail:"assets/MarkyTube.png",
      Content:"Test Title Movie 2",
      VidId:2},
      {FileName:"bb_bunny", 
      Path:"assets/bb_bunny.webm",
      VidType:"webm", 
      Thumbnail:"assets/MarkyTube.png",
      Content:"Test Title Movie 3",
      VidId:3},
      {FileName:"bb_bunny", 
      Path:"assets/bb_bunny.webm",
      VidType:"webm", 
      Thumbnail:"assets/MarkyTube.png",
      Content:"Test Title Movie 4",
      VidId:4}
    ];
  }

  GetTvSeries():IVideo[]{
    return this._VideoList = [
      {FileName:"mov_bbb", 
      Path:"assets/mov_bbb.mp4",
      VidType:"mp4", 
      Thumbnail:"assets/MarkyTube.png",
      Content:"Test Title TV 1",
      VidId:1},
      {FileName:"bb_bunny", 
      Path:"assets/bb_bunny.webm",
      VidType:"webm", 
      Thumbnail:"assets/MarkyTube.png",
      Content:"Test Title TV 2",
      VidId:2},
      {FileName:"bb_bunny", 
      Path:"assets/bb_bunny.webm",
      VidType:"webm", 
      Thumbnail:"assets/MarkyTube.png",
      Content:"Test Title TV 3",
      VidId:3},
      {FileName:"bb_bunny", 
      Path:"assets/bb_bunny.webm",
      VidType:"webm", 
      Thumbnail:"assets/MarkyTube.png",
      Content:"Test Title TV 4",
      VidId:4}
    ]
  }
  
  GetExistingVideoList():IVideo[]{
    return this._VideoList;
  }
}
