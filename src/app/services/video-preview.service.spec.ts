import { TestBed } from '@angular/core/testing';

import { VideoPreviewService } from './video-preview.service';

describe('VideoPreviewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VideoPreviewService = TestBed.get(VideoPreviewService);
    expect(service).toBeTruthy();
  });
});
