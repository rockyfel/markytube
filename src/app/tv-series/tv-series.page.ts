import { Component, OnInit } from '@angular/core';
import { isNull, isNumber } from 'util';
import { VideoPreview } from '../VideoPreview';
import { IMyVideo } from '../interfaces/IMyVideo';
import { IVideo } from '../interfaces/IVideo';
import { VideoService } from '../services/video.service';

@Component({
  selector: 'app-tv-series',
  templateUrl: './tv-series.page.html',
  styleUrls: ['./tv-series.page.scss'],
})
export class TvSeriesPage extends VideoPreview implements OnInit {
  public TvSeries: IMyVideo;
  constructor(public videoService: VideoService) { 
    super();
  }

  ngOnInit(){
    this.GetTvSeries();
    this.videoService.SelectedVideo = null;
  }

  GetTvSeries(){
    this.TvSeries = {
      VideoList: this.videoService.GetTvSeries()
    }
  }

}
