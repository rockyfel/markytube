import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TvSeriesPage } from './tv-series.page';
import { PlayVideoComponent } from '../play-video/play-video.component';

const routes: Routes = [
  {
    path: '',
    component: TvSeriesPage,
    children: [
      {
        path: ':id',
        component: PlayVideoComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TvSeriesPageRoutingModule {}
